# Java

# for文
https://qiita.com/masa-kunikata/items/72703085dbf59cc83052


# キャスト（型変換）

## 文字列 → 数値

### int型
```
Integer.parseInt("1");
```

### Integer型
```
Integer.valueOf("1");
```

※下記のようにnullは変換できない
```
Integer.valueOf(null);
```

## 数値 → 文字列

### int型

```
String.valueOf(num);
```

### Integer 型

```
integer.toString();
```

# コンスタラクタ

PHPと違って**継承されない**ので注意！

ただし、例外で暗黙的な呼び出しは起こる

## 暗示的な呼び出し

```java
class AaaClass {
    // コンストラクタ書いてなくても空のコンストラクタが呼ばれている
    /*
    public AaaClass() {
    }
    */
}

```

## 暗黙的な呼び出し、継承

```java
class SuperClass {
    public SuperClass() {
        // なにかの処理
    }
}

class SubClass extends SuperClass {
    // コンストラクタがなくても暗黙的に下記のコンストラクタが書かれているのと同じ挙動をする
    /*
    public SubClass() {
        super();
    }
    */
}
```


# オーバーロード, overload

- 同じメソッド名で引数が異なるものを複数定義できる
- abstract で引数を可変で定義することはできない
- 戻り値は同じ型にしないといけない

```
    public int aaa() {
        return 0;
    }
    public int aaa(int num) {
        return 100/num;
    }

```

# 配列

## 初期化

```
String[] strs = new String[] {"bbbbb", "cccc", "ddddd"}
```

宣言と同時だと下記のように省略できる

```
String[] strs = {"bbbbb", "cccc", "ddddd"}
```

## 文字列に変換

```
int[] intValues = {1111, 2222, 3333}

// 変換
Arrays.toString(intValues)
```

上記だと下記の値が出力される

```
[111, 222, 333]
```

# List

## 初期化

1行で書くときは下記

```
List<String> aaa = new ArrayList<>(Arrays.asList("aaaa", "bbb", "cccc"));
```

下記は、新規でリストに値を追加できないので注意
```
List<String> bbb = Arrays.asList("aaaa", "bbb", "ccc");
```

# Map
## 初期化

```
Map<Integer, Integer> ondoKbnSyomikigenLength = new HashMap<Integer, Integer>() {{
    put(1, 8);
    put(2, 8);
    put(3, 6);
    put(null, 8);    // nullもいける
}};
```

# 日付
## Java8から新しい
- LocalDateTime (Date の代わり)
- DateTimeFormater

https://qiita.com/tag1216/items/91a471b33f383981bfaa

## Java8以前

文字列 → Date → Calendar に変換して扱うのが一番良い。

文字列から `Calendar` へは直接変換できない。

- Date
- SimpleDateFormat

### 文字列 → Date
```
// simpleDateFormat を作成
final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault());

// 存在しな日付はエラーにする
simpleDateFormat.setLenient(false);       

// Date 型へ
Date date = simpleDateFormat.parse(yyyyMm);
```

### Date → Calendar

```
final Calendar calendar = Calendar.getInstance();

calendar.setTime(date);
```

### Date → 文字列

```
// 今日の日付
Date date = new Date(); 
SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
String strDate = dateFormat.format(date);
```

# 例外, エラー, Exception, Error

## 継承
```
Throwable
    Error // プログラムで捕捉すべきではない
        OutOfMemoryError
        ....
    Exception // 検査例外, プログラム中で捕捉しなければならない
        IOException
        SQLException
        ....
        RuntimeException // 実行時例外, プログラムの中で補足しなくてもいい, 実行時例外をどこでも捕捉しなければ、最終的に例外が発生したスレッドが終了します。
            NumberFormatException
            ArithmeticException
            OutOfBoundsException
                ArrayIndexOutOfBoundsException
            ClassCastException
            IllegalArgumentException
                NumberFormatException
            IllegalStateException
            NullPointerException
            ....
```

## 参考

- [Javaの例外処理について](https://qiita.com/k4h4shi/items/2c39edaeef1c92f6644a)

