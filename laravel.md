# Laravel


# リンク集

- [Laravel - 公式](https://laravel.com/)
- [Document（ドキュメント）- ReadDouble](https://readouble.com)・・・こっちのが見やすい
- [APIドキュメント(8.x)](https://laravel.com/api/8.x/)

## 用語

- LTS・・・Long Term Support（LTS）


# 基本

## 流れ

1. ユーザーのリクエストがwebサーバーへ到着
1. public/index.phpを実行
1. composerから出力されたvendor/autoload.phpが実行 → クラスマップの情報を取得
1. bootstrap/app.phpを実行
1. Illuminate\Foundation\Applicationがnewされインスタンス化
1. Kernelが生成され、HTTPリクエスト情報からRequestインスタンスを生成しKernel->handle()メソッドへ渡し処理
1. 実行結果をユーザーへレンスポンス

## ディレクトリ構成

|ディレクトリ|役割|
|:---|:---|
|app/Http/Kernel.php|グローバルミドルウェアの登録、ミドルウェアのグループ登録|
|app/Http/Controllers/|コントローラー|
|app/Http/Middleware/|ミドルウェア|
|app/Providers/|サービスプロバイダ、DI|
|config/app.php|configファイル、サービスプロバイダの登録|
|routes/|ルーター、ミドルウェアの登録|
|routes/api.php|api用|
|routes/channels.php|ブロードキャストチャンネル、WEBソケットとか|
|routes/console.php||
|routes/web.php|通常はこれ、WEB用|
|vendor/|ライブラリ|


## フレームワーク名前空間構成

|名前空間|役割|
|:---|:---|
|Illuminate\Foundation\Application|大本のapp、$this->app|
|Illuminate\Support\Facades|ファサード|

`vendor/laravel/framework/src/` がライブラリのディレクトリ

# ルーティング、ミドルウェア

[Laravel 7.x ルーティング - 公式](https://readouble.com/laravel/7.x/ja/routing.html)

## ファイル

`routes/` に全てルーティングの設定が入っている。

## 基本ルーティング

### 直接ビューを返す

```php
Route::get('posts/{post}/comments/{comment}', function ($postId, $commentId) {
    return "post id: {$postId}, comment id: {$commentId}";
});
```

`/posts/1/comments/2` のULRの場合は、クロージャーの引数が `$postId = 1`, `$commentId = 2` の値になる

### コントローラーに値を渡す

```php
// Laravel 8から
Route::get('/user/{user_id}', [UserController::class, 'index']);
    ->where('user_id', '[0-9]+')    // {user_id} は数字、それい外だと 404
    ->name('user.index');


// Laravel 7まで
Route::get('/user/{userId}', 'UserController@index');
    ->where('user_id', '[0-9]+')    // {user_id} は数字
    ->name('user.index');
```

コントローラの方の書き方は下記

`UserController` コントローラーの `index` アクションの場合

```php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request, $user_id)
    {
        $data = [....];
    
        return view('user.index', $data);
    }

}
```

### get, post などをまとめて指定できる

#### ルーターでの書き方

```php
// リストと詳細のみのとき
Route::resource('photos', 'PhotosController');
```
#### 上記だと下記のコントローラーなど

|HTTPメソッド|URI|Action|Route Name|機能|
|:---|:---|:---|:---|:---|
|GET|/photos|index|photos.index|一覧、リストページ|
|GET|/photos/{photo}|show|photos.show|詳細ページ|
|GET|/photos/create|create|photos.create|新規作成ページ|
|POST|/photos|store|photos.store|新規作成処理|
|GET|/photos/{photo}/edit|edit|photos.edit|更新ページ|
|PUT/PATCH|/photos/{photo}|update|photos.update|更新処理|
|DELETE|/photos/{photo}|destroy|photos.destroy|削除処理|

#### 上記の何個かだけに限ることができる

```php
// リストと詳細のみのとき
Route::resource('printing_schedules', 'PrintingScheduleController')
    ->only(['index', 'show']);
```
        
## ミドルウェア

[Laravel 7.x ミドルウェア - 公式](https://readouble.com/laravel/7.x/ja/middleware.html)

```php
protected $routeMiddleware = [
    'auth' => \App\Http\Middleware\Authenticate::class,
    'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
    ....
];
```

ミドルウェアパラメータ

## ルートグループ

```php
Route::middleware(['first', 'second'])->group(function () {
    Route::get('/', function () {
        // firstとsecondミドルウェアを使用
    });

    Route::get('user/profile', function () {
        // firstとsecondミドルウェアを使用
    });
});
```

## 名前

```php
Route::get('user/{id}/profile', function ($id) {
    //
})->name('profile');

// url取得
$url = route('profile', ['id' => 1, 'photos' => 'yes']);

// /user/1/profile?photos=yes
```

# 環境変数

## 設定

`.env` で下記のように記述

```
# AAA という環境変数に value という値を設定
AAA=value
BBB=1111

# laravelでの環境の読み分けで使われる 環境変数
APP_ENV=local
```


## 取得

`AAA` という環境変数を取得するとき、下記コードで取得できる。

config ファイルを経由して取得すること。

|laravel（config経由、推奨）|laravel（直接、非推奨）|PHP|
|---|---|---|
|config('app.aaa');|env('AAA', 'default_value');|$_ENV('AAA');|


※config 経由の場合は `config/app.php` に下記を記述したとき

```
return [
    ... 略 ...
    'aaa' => env('AAA', 'default_value'),
    ... 略 ...
];
```

### APP_ENV の取得

#### 取得

下記の方法でも取得できる

```
App::environment()
```

#### 

```php
if (App::environment('local')) {
    // 環境はlocal
}

if (App::environment(['local', 'staging'])) {
    // 環境はlocalかstaging
}
```


# Blade

## データ（変数）表示

### エスケープする

```
{{ $aaa }}
```

### エスケープしない

```
{!! $aaa !!}
```


# 設定、config

`config/zycos.php` ファイルが下記だと
```
return [
    'name' => 'Kato Shunsuke',
];

```

下記で `Kato Shunsuke` が取得できる
```
config('zycos.name');
```


# ファサード

staticインターフェースを提供

下記メソッドなど

```
Cache::get('key');
View::make('profile');
```

なるべく使わない方がよい

# データベース関連

## 初期作成の流れ

### モデル、マイグレーションファイル、シーダーを一度に作成

```
php artisan make:model ModelName --migration --seed
 
# ↑の省略形
php artisan make:model ModelName -m -s
```

上記だと下記のファイルが一度に作成される

||ファイル名||
|---|---|---|
|モデル|ModelName.php|単数形、指定したまま|
|マイグレーション|2020_01_01_create_model_names_table.php|自動で複数形|
|シーダー|ModelNameSeeder.php|単数形|

### モデルのみ

```
php artisan make:model ModelName
```

### マイグレーションファイルのみ

```
php artisan make:migration create_table_name_table
```

ORMクラスのテーブル名はモデル名から自動で作成してくれるので不要

### 状態確認

```
php artisan migrate:status
```

### シーダーの作成、登録、実行

#### 作成

```
php artisan make:seeder TableNameSeeder
```

`database/seeds/TableNameSeeder.php` に作成される



#### 登録1

`php artisan db:seed` コマンドで実行されるようにするには、 `database/seeds/DatabaseSeeder.php` に追加しないといけない


```
class DatabaseSeeder extends Seeder
{
    public function run()
    {
        ....（略）...
        
        $this->call(TableNameSeeder::class);
    }
}

```

#### 登録2

Composerのオートローダを再生成

```
composer dump-autoload
```

[laravelでphp artisan db:seed すると does not exist とエラーが出る](https://qiita.com/teneye/items/1fdedd8ffe5a5ebd6c71)

#### 実行

```
# 全て実行
php artisan db:seed

# 指定のシーダーを実行
php artisan db:seed --class=TableNameSeeder
```


## マイグレーションコマンド



### 状態確認

```
php artisan migrate:status
```

### up()

```
# 通常、マイグレーション実行していないもの（Run?=No）を実行
php artisan migrate

# 指定ファイルのみup
php artisan migrate --path=database/migrations/2020_09_30_115222_create_trn_receives_table.php
```

### down()

```
# 通常、1ステップだけ
php artisan migrate:rollback

# 指定ファイルのみdown()
php artisan migrate:rollback --path=database/migrations/2020_09_30_115222_create_trn_receives_table.php

# ステップ数指定
php artisan migrate:rollback --step=5   # 5ステップバック
```

```
# 全て
php artisan migrate:reset
```

### down() → up()

```
# 全て、スッテップ数（batch）は全て1になる
php artisan migrate:refresh

# ファイル指定
php artisan migrate:refresh --path=database/migrations/2020_09_30_115222_create_trn_receives_table.php
```

### DROP → up()

`down()` でエラーが出る場合はこちら。

```
# 全て、STEP数は全て1になる
php artisan migrate:fresh

# さらにseedも同時に実行
php artisan migrate:fresh --seed
```

# ORM, Eloquent

## リレーション


### hasOne

`User` が１つだけの `Phone`　を所持する

```
class User extends Model
{
    /**
     * phone はリレーション名
     */
    public function phone()
    {
        // 第一引数は公式サイトでは下記の値になってますが `App\Models\Phone::class` の方がよいかもしれない
        // 第2引数と第3引数は省略可能、
        return $this->hasOne(App\Models\Phone:class, 'phone_id', 'id');
    }
}
```

`$user->phone` でたどれる。

予めJOINでまとめて取得するようなクエリにする場合は、下記コードでできる

```
$users = User::with('phone')->get();
```

### belongsTo

`User` が複数の `Phone`　を所持する

JOINが `phones.user_id = users.id` のとき

`$phone->user` でPhoneモデルからUserモデルに辿れる

```
class Phone extends Model
{
    /**
     * phone はリレーション名 
     */
    public function user()
    {
        // 第1引数:公式サイトでは下記になっているが `App\Models\User::class` の方がよいかも
        // 第2引数: phones.user_id、このモデルのキー、省略可能
        // 第3引数: users.id、辿り先のモデルの主キー、省略可能
        return $this->belongsTo(App\Models\User::class, 'user_id', 'id');
    }
}
```

### hasMany

`Post` が複数の `Comment`　を所持する

JOINが `posts.id = comments.post_id` のとき


```
class Post extends Model
{
    /**
     * comments はリレーション名
     */
    public function comments()
    {
        // 第1引数:公式サイトでは下記になっているが `App\Models\Comment::class` の方がよいかも
        // 第2引数: comments.post_id、CommentモデルのPostへのキー、省略可能
        // 第3引数: posts.id、こののモデルの主キー、省略可能
        return $this->hasMany(App\Models\Comment::class, 'post_id', 'id');
    }
}
```

`$post->comments` でたどれる、恐らくコレクション


# シーダー、ファクトリ

## 覚書

- ファクトリは基本的に1つのモデル（またはテーブル１行）を作成するためのもの
- シーダーはテーブルにINSERTするためのもの
- 下のリレーションの階層がある（hasManyの）場合はシーダーでする
- 上のリレーションの階層がある（belongsToの）場合はファクトリでする

## シーダー

### ファクトリ未使用

```php
class LeafSeihanSeeder extends Seeder
{
    public function run()
    {
        $rows = [
            [
                'id' => (int),
                'name' => (string),
                ....
            ],
            [...],
            ...
        ];
        
        // created_at, updated_at を入れる
        $rows = array_map(function($row) {
            return $row + [
                'created_at' => now(),
                'updated_at' => now(),
            ];
        }, $rows);
        
        // バルクインサート
        DB::table( (new LeafSeihan)->getTable() )->insert();
        // LeafSeihan::insert($rows);   // こっちでもOK、公式非掲載
    }
}
```

### ファクトリ使用

```php
class LeafSeihanSeeder extends Seeder
{
    public function run()
    {
        // ３つのモデルを作成して、3行分をDBに保存
        User::factory()->count(3)->create();    // 8.0 から
        // factory(App\User::class, 3)->create(); // 7.x まで
    }
}
```

## ファクトリ

```php
$factory->define(LeafSeihanProofItem::class, function (Faker $faker) {
    $quantity = $faker->randomFloat(1, 0, 10000);
    $unit_cost = $faker->randomFloat(1, 0,100);

    return [
        'proof_header_code' => $faker->numberBetween(1, 100), // 親テーブルの key_code なので適当
        'person_code' => ...,
        'item_code' => ...,
        'size_code' => ...,
        'size_name' => $faker->sentence,
        'quantity' => $quantity,
        'unit_cost' => $unit_cost,
        'amount' => $quantity * $unit_cost,
    ];
});

```

## 下のリレーションの階層がある場合

シーダー

```php
class LeafSeihanSeeder extends Seeder
{
    // 3段階層
    public static function createWithProofAndItems(int $amount = 1) {
        // create() で eloquentのCollectionが返ってくる
        return factory(LeafSeihan::class, $amount)->create()->each(function($seihan) {
            // saveMany() はCollectionが引数
            $seihan->proofs()->saveMany(
                factory(LeafSeihanProof::class, rand(1, 5))->create()->each(function($proof) {
                    $proof->items()->saveMany(
                        factory(LeafSeihanProofItem::class, rand(1, 5))->create()
                    );
                })
            );
        });
    }
    
    public function run()
    {
        self::createWithProofAndItems(10);
    }
}
```



# バリデーション

コントロラーに直接書く方法とフォームリクエストクラスに書く方法がある

フォームリクエストの書くほうがよい

[Laravelのバリデーションで指定できる内容をざっくりまとめ直しました。 - Qiita](https://qiita.com/fagai/items/9904409d3703ef6f79a2)

`$validator->validate()` でバリデーションを実行すると自動で前のページにリダイレクト

[LaravelのFormRequestクラス機能まとめ - Qiita](https://qiita.com/OKCH3COOH/items/53db8780027e5e11be82)

## フォームリクエストクラス作成コマンド

```
php artisan make:request AaaRequest
```

`app/Http/Requests/AaaRequest.php` に作成される

## 配列

```
// zycos_temp_ids が必須でint型でないといけない場合
[
    'zycos_temp_ids' => 'required|array',
    'zycos_temp_ids.*' => 'integer',
];
```

参考リンク: https://readouble.com/laravel/master/ja/validation#validating-arrays