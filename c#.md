# C#

ひとまず、普通のC#とVisual C# をごちゃまぜで書きます

# 型

## string

```cs
string aaa = "bbb";
aaa = "ccc";    // ここは新しいインスタンスを生成する
```

# if

```
if (result == DialogResult.Yes)
{
    //
}
else if (result == DialogResult.No)
{
    //
}
else {
    //
}
```

# foreach

```
foreach (string str in strs) 
{
    //
}
```

# 配列、コレクション
「配列」は固定長なので使わない方が良い

## 中身何でもOK
```
ArrayList aaas = new ArrayList();
aaas.Add("aaaaaa");
aaas.Add(100);  // 違う型も入れられる

Console.WriteLine(aaa[0]);  // コンソールに表示
Console.WriteLine(aaa[1]);  // コンソールに表示
```

## 中身の型は固定
```
List strs = new List<string>();
strs.add("aaaaa");
strs.add("bbbbb");
```


# クラス定義

## クラス

### 基本

```
namespace Kato {
    // public
    public class AaaXxx {
        ....
    }
    
    // public のプログラム内限定、基本これがよい
    internal class BbbXxx {
        ....
    }
    
    // internal と同じ
    class CccXxx {
        ...
    }
    
    // static もできる
    static class DddXxx {
        ....
    }
}
```

### 継承

```
namespace Menu
{
    class Aaa : Bbb 
    {
        ....
    }
}
```

### クラスを別ファイルに分割できる、partial

```
// FrmMenu.cs
namespace Menu
{
    // partial をつける
    public partial class FrmMenu : Form
    {
        ....
    }
}
```

```
// FrmMenu.Designer.cs ファイル

// 同じ名前空間
namespace Menu
{
    // parcial をつける、同じ public
    public partial class FrmMenu
    {
        ....
    }
}
```

## フィールド

```
namespace Kato 
{
    class Aaa 
    {
        // public 
        public string userName;
        
        // public のプログラム内限定、基本これがよい
        internal string userName2;
        
        // protected
        protected string userName3;
        
        // protected のプログラム内限定、基本これがよい
        protected internal string userName4;
        
        // private
        private string userName5;
        
        // 省略時は private
        string userName6;
        
        // static もできる
        static private int numberOfXxxx;
        
        // クラス定数
        public const int USER_POST_NUMBER = 1230001;
    }
}
```

## プロパティ

プロパティ名は **パスカル（アッパーキャメル）記法** で書くのが習わし

### 基本

```
namespace Kato 
{
    clas Aaa 
    {
        // これはフィールド、プロパティで使うので private がよい
        private string userName;
        
        // プロパティ、public などの修飾子は プロパティと同じ
        public string UserName
        {
            get { 
                return userName + "様";  
            }
            
            // getはそのままpublic、setだけprotected などできる
            protected set {
                userName = value; 
            }
        }
    }
}
```

### 省略形、自動実装プロパティ

```
namespace Kato 
{
    clas Aaa 
    {
        // プロパティのゲッター、セッターが自動で定義される。フィールドも内部的に作成される
        public string UserName { get; set; }
        
        // 初期化も同時にできる
        public string UserName2 { get; set; } = "Suuuuun";
    }
}
```

## メソッド

メソッド名は **パスカル（アッパーキャメル）記法** で書くのが習わし

```
namespace Kato 
{
    clas Aaa 
    {
        // public の修飾子はプロパティと同じ、デフォルト値も設定できる
        public int GetDaemonKakkaAge(int age = 0) 
        {
            return age + 100000;
        }
    }
}
```

## コンストラクタ

```
namespace Kato 
{
    class Aaa 
    {
        // コンストラクタ、省略もできる
        public Aaa(int age, string name)
        {
            ....
        }
        
        // メソッド
        public int calcAge()
        {
            ....
        }
    }
}
```




# 構造体

| |値／参照|速度|継承|
|---|---|---|---|
|構造体|値型|速い|×|
|クラス|参照型|普通|○|

構造体は値型


# 例外

```
object o2 = null;
try
{
    int i2 = (int)o2;   // Error
}
catch (Exception e) when (e.ParamName == "aaa") // 例外のパラメータで絞り込める
{
    ....
}
finally
{
    ....
}
```




# メッセージボックス（アラートalert、ダイアログdialog） を表示

## 1つだけ（OKのみ）
```
MessageBox.Show("本文ですよ", "タイトル");
```

# YesとNo（はい、いいえ）

```
DialogResult result = MessageBox.Show("YesかNoか！", "タイトル", MessageBoxButtons.YesNo);
if (result == DialogResult.Yes)
{
    // YESのときの処理
}
else if (result == DialogResult.No)
{
    // Noのときの処理
}
```

# 新しいウインドウを開く

```
Form2 form2 = new Form2();
form2.Show();
```

# ログ出力（出力ウインドウに表示）

[参考リンク - 公式](https://docs.microsoft.com/ja-jp/visualstudio/debugger/diagnostic-messages-in-the-output-window?view=vs-2019)

## デバッグのみ出力

```
System.Diagnostics.Debug.WriteLine("ffff");
```

## デバッグとリリース両方出力

```
System.Diagnostics.Trace.WriteLine("ffff");
```

