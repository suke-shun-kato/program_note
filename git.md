# Git

# コミットメッセージのベストプラクティス

```
feat: 〇〇なため、△△を追加
```

## プレフィックス

- feat: 新しい機能
- fix: バグの修正
- docs: ドキュメントのみの変更
- style: 空白、フォーマット、セミコロン追加など
- refactor: 仕様に影響がないコード改善(リファクタ)
- perf: パフォーマンス向上関連
- test: テスト関連
- chore: ビルド、補助ツール、ライブラリ関連

## 参考

[僕が考える最強のコミットメッセージの書き方 - Qiita](
https://qiita.com/konatsu_p/items/dfe199ebe3a7d2010b3e)


# 初期設定

[gitの初期設定 - Qiita](https://qiita.com/suke/items/041cb9c66af96370d51a)

# git log Windows（コマンドプロンプト）で文字化け

[Windows10のgit logが文字化け（<E8><A4>...)するときの対処](https://qiita.com/Tachibana446/items/b6a869afa9959581dfc0)


# 間違ったブランチ名でpushしてしまったとき

```
# リモートブランチ削除
git push origin :feature/old

# リモートブランチ関連付け
git push -u origin feature/new
```

# Windows で `git add` や `git stash` したときに下記の警告が出たとき

```
warning: LF will be replaced by CRLF in laravel/.editorconfig.
The file will have its original line endings in your working directory
```

`.gitattributes` で `* text=auto` など設定されていて、git config の設定でも core.eol が設定されていなくて crlf と設定されている場合など

`.gitattributes` を `* text lf` と設定、もしくは git config の設定を core.eol=lf と設定すればなおる

# .gitattributes

全てのファイルに、`text` 属性に `auto` を設定
```
*           text=auto
```

txtファイルに、`text` 属性に `true` を設定
```
*.txt       text
```

vcprojファイルに、`text` 属性に `true` を、`eol` 属性に `crlf` を設定
```
*.vcproj    text eol=crlf
```

jpg ファイルに、`text` 属性に `false` を設定
```
*.jpg       -text
```

# コミット日時を変更

https://qiita.com/yoichi22/items/b25d223b639621b834cb

# git push

## リモートブランチを削除

```
git push -d origin feature/aaa

# これでもOK
git push --delete origin feature/aaa

# こっちでもOK
git push origin :feature/aaa
```

## 途中までのコミットをpushする

下記のコマンドでpushできる

```
git push <repository> <ｓｒｃ>:<dst>
```

一つ前のコミットを origin リポジトリの feature/xxx ブランチにpush

```
git push origin HEAD~:feature/xxxx
```

foo ブランチを origin リポジトリの bar ブランチにpush

```
git push origin foo:bar
```

# git rm

## 単純にファイル削除

```
git rm <ファイル名>
```

## 管理対象外にする、PC上にファイルは残してGit上（ワーキングツリー）とadd（インデックス）からは削除

```
git rm --cached <ファイル名>
```

# git config

## local, global, system

| |対象|
|:---:|:---|
|local|対象リポジトリ内|
|global|ログインユーザー|
|system|システム（PC）全体|

## 設定一覧表示

全部

```
git config -l

# リポジトリ内だけの設定を見たい場合は
git config -l --local
```

キー名指定

```
git config キー名
```

## 登録、更新

```
#ローカルリポジトリに設定
git config キー名　設定値

# 例
git config --global core.autocrlf false
```

## 削除

```
git config --unset キー名
```


# sshの設定

## SSH鍵の生成

```
ssh-keygen -t rsa -f (ファイル名)
```

- -t 鍵作成アルゴリズム（RSA形式）
- -f 鍵ファイル名
※最終的に「~/.ssh」に鍵を置くので「~./ssh」で上記コマンドを実行した方が良い

パスフレーズは設定しなくても良いのでEnter2回

- 秘密鍵：(ファイル名)
- 公開鍵：(ファイル名).pub
が作成される

## 公開鍵を設定

SSH鍵の中身の文字列をコピー(mac)

```
pbcopy < ~/.ssh/(鍵ファイル名).pub
```

GitHubやBitbucketなどの設定ページで上記を貼り付ける

## ホストの設定

~/.ssh/config を下記のように書くと、接続先別に使用する「鍵」と「ユーザー名」を設定できる

```
Host bitbucket.org
  IdentityFile ~/.ssh/bitbucket
  User git

Host github.com
  IdentityFile ~/.ssh/github
  User git

```

- Host - 接続先のSSHのアドレス
- IdentityFile - 鍵のファイルパス
- User - 接続先のUser名

例えば１番目だと `git@bitbucket.org` が接続先で、鍵の場所が `~/.ssh/bitbucket` である


## SSHで繋がるかの確認

### GitHubの場合

```
ssh -T git@github.com
```

### Bitbucketの場合

```
ssh -T git@bitbucket.org
```




