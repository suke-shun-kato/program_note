# PostgreSQL

# 参考リンク

[PostgreSQLの使い方 - DBOnline](https://www.dbonline.jp/postgresql/)

# 型

postgreSQL では unsigned 型はない

## SERIAL型

MySQLではオートインクリメントみたいなもの


## NUMERIC型

任意の精度を持つ数

### 例

```
# 全体が5桁で小数点以下は2桁、-999.99 〜 999.99
NUMERIC(5, 2)
```

### 例2

```
# 全体が6桁で小数点以下は0桁、-999999 〜 999999
NUMERIC(6)
```


## DECIMAL型

NUMERIC型 と同じ


## bpchar型

`character(n)` 型の内部的な名前

### よくある使い方

```
WHERE sex = 'm'::bpchar
```

`::` は型のキャスト

`m` を `bpchar` 型にキャスト


# 主キーを設定

```
CONSTRAINT key_name  PRIMARY KEY
```

## よくある主キーの CREATE 文

```
CREATE TABLE tasks (
    id SERIAL NOT NULL CONSTRAINT key_name  PRIMARY KEY,
);
```

# スキーマ変更

よくある型の変換のSQL

```
ALTER TABLE table_name ALTER COLUMN column_name TYPE integer USING column_name::integer
```

# DBにログイン

コマンドでログインするには下記でログインできる

```
psql -h ホスト名 -p ポート番号 -U ロール名（ユーザー名） -d データベース名
```

初期設定で設定されている値だと下記になる

```
psql -h localhost -p 5432 -U postgres -d postgres
```

ポート番号、ロール名、データベース名は初期設定だと省略できるので、下記でログインできる

```
psql -U postgres
```
