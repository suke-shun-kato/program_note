# 全般

## タブを複数行表示にする

`Editor > General > Editor Tabs > Appearance > Show tabs in one row` を外す

## 1:Project などの数字を表示

`Appearance & Behavior > Appearance > Tool Windows > Show tool windows numbers`

# PhpStorm

## キャッシュクリア, 遅いとき

`File > Invalidate Caches / Restart > Invalidate and Restart`

## .idea

JetBrain のエディタの設定関連のパラメータを保存している

## ショートカット

| |mac|windows|
|:---:|---:|---:|
|対応括弧へ移動|control + M||
|行ジャンプ|command + L|Ctrl + G|
|単語選択|option + ↑||
|メソッド選択|option + ↑ -> ↑||
|複数行選択（マウス、飛び飛び）|option + クリック|Alt + クリック|
|複数行選択（マウス、連続）|shift + option + ドラッグ|shift + Ctrl + ドラッグ|
|複数行選択（キーボード）|option -> option -> ↑ or ↓|Ctrl -> Ctrl -> ↑ or ↓||
|左のタブへ移動|command + shift + [|Alt + ←|
|右のタブへ移動|command + shift + ]|Alt + →|
|次のスプリッター（タブグループ）へ移動|option + tab||
|前のスプリッター（タブグループ）へ移動|option + shift + tab||
|一つ前の操作位置へ戻る|command ＋ [||
|   |command ＋ option ＋ ←||
|一つ先の操作位置へ戻る|	command ＋ ]||
|   |command ＋ option ＋ →||

# DataGrip

## 色を付ける

### 色々設定できる
- 接続先右クリック -> Properties -> Nameフォームの右端の「○」

### 色の設定だけ
- 接続先右クリック -> Color Settings