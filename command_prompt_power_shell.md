# コマンドプロンプト

# ヘルプ

```
コマンド /?
```

# ファイル削除(del)

```
DEL ファイル名
```

# ファイルの中身を空にする（文字列削除）

## コマンドプロンプト

```
type nul > storage\logs\laravel-2021-06-09.log
```

## PowerShell の場合

```
echo $null > laravel\storage\logs\laravel-2021-03-25.log
```

# grep
```
FIND 文字列
```

# パイプライン(|)

```
コマンド | コマンド
```

# 文字コード

windows10は標準でShift-Jisみたい

## UTF-8への変更

[Windowsコマンドプロンプト　文字コード設定](https://qiita.com/user0/items/a9116acc7bd7b70ecfb0)