# Docker

# Dockerfile からイメージ作成→コンテナ作成して起動

WEBサーバー用の Dockerfile だと下記の手順でできる

## ビルドしてイメージ作成

```
# -t イメージ名を指定
# . は Dockerfile があるディレクトリパス
docker build -t test_web:1.0 .

# イメージ確認
docker image ls
```

## イメージからコンテナ起動

```
docker run -d -p 8080:80 test_web:1.0

# コンテナ確認
docker container ls
```

## 起動確認

下記URLでサーバーにアクセスできる

```
http://127.0.0.1:8080/
```

# ファイルをコンテナ内でも使えるようにする

Dockerfile の ADD と COPY の違いを結論から書く(https://qiita.com/YumaInaura/items/1647e509f83462a37494)


## COPY

- リモートからのファイル追加は出来ない
- 圧縮ファイルは自動解凍されない

※基本これ

## ADD

- リモートからもファイル追加できる
- 圧縮ファイルが自動解凍される



## volumes



# ログ

## まとめて

```
docker-compose logs -f
```

## コンテナ別

```
docker logs -f CONTAINER
```

# Windows(WSL)

## メモリ枯渇問題

https://qiita.com/yoichiwo7/items/e3e13b6fe2f32c4c6120

## 再起動

```
wsl.exe --shutdown
```